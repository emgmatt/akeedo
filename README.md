# Akeedo
---

A script that detects a query string form a URL and swaps out HTML elements and attributes. Great for use with social or email marketing campaigns to build single landing pages with customized content for multiple audiences with URL Query Strings.

Can be customized for other purposes as well. Open Sourced for you to do with what you want.
